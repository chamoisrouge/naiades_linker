# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'setParams.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(491, 341)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(40, 280, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.mMapLayerComboBoxStation = gui.QgsMapLayerComboBox(Dialog)
        self.mMapLayerComboBoxStation.setGeometry(QtCore.QRect(170, 40, 160, 32))
        self.mMapLayerComboBoxStation.setObjectName("mMapLayerComboBoxStation")
        self.mFieldComboBoxId = gui.QgsFieldComboBox(Dialog)
        self.mFieldComboBoxId.setGeometry(QtCore.QRect(170, 90, 160, 32))
        self.mFieldComboBoxId.setObjectName("mFieldComboBoxId")
        self.startDate = QtWidgets.QDateEdit(Dialog)
        self.startDate.setGeometry(QtCore.QRect(130, 140, 110, 32))
        self.startDate.setObjectName("startDate")
        self.endDate = QtWidgets.QDateEdit(Dialog)
        self.endDate.setGeometry(QtCore.QRect(130, 190, 110, 32))
        self.endDate.setObjectName("endDate")
        self.comboBoxThematic = QtWidgets.QComboBox(Dialog)
        self.comboBoxThematic.setGeometry(QtCore.QRect(130, 230, 83, 32))
        self.comboBoxThematic.setObjectName("comboBoxThematic")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 50, 131, 18))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(10, 100, 101, 18))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(10, 150, 101, 18))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(10, 190, 101, 18))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(10, 240, 101, 18))
        self.label_5.setObjectName("label_5")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Couche des stations"))
        self.label_2.setText(_translate("Dialog", "Champ avec ID"))
        self.label_3.setText(_translate("Dialog", "Start date"))
        self.label_4.setText(_translate("Dialog", "End date"))
        self.label_5.setText(_translate("Dialog", "Thématique"))

from qgis import gui
