<a href="http://naiades.eaufrance.fr/"><img alt="Logo Naïades" src="http://naiades.eaufrance.fr/sites/all/themes/naiades_theme/images/logo-naiades.png" height="50"></a> <a href="https://ofb.gouv.fr/"><img alt="Logo OFB" src="https://ofb.gouv.fr/sites/default/files/logo-ofb.png" height="80"></a> <a href="https://www.eaufrance.fr/"><img alt="Logo EauFrance" src="https://www.eaufrance.fr/sites/default/files/2020-02/Logo_eaufrance_web.png" height="55"></a> <a href="https://www.cnrs.fr/"><img alt="Logo CNRS" src="https://www.cnrs.fr/themes/custom/cnrs/logo.svg" height="80"></a> <a href="https://www.fire.upmc.fr/?q=fr/"><img alt="Logo FIRE" src="https://www.fire.upmc.fr/sites/default/files/logo_fire_fond_transparent.png" height="80"></a>
# Naïades Linker
Ce plugin "**Naïades Linker**" permet de télécharger les données relatives à la qualité des eaux de surface continentales depuis la base de données nationales [Naïades](http://naiades.eaufrance.fr/).

Ce plugin propose simplement un interface de téléchargement de ces données depuis QGIS, ce qui offre une ergonomie supplémentaire. Tout repose sur l'API développée par les gestionnaires de Naïades, qui permet de télécharger les données par conruction d'une URL logique. Ce plugin permet à l'utilisateur de construire cette URL  à partir de QGIS en sélectionnant les stations, les périodes et les paramètres de son choix.

# Utilisation
## Les stations de mesures
La couche SIG des stations de mesures est librement accessible et téléchargeable depuis [le site du Sandre](http://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/71767e88-a021-4e88-8787-5feed04958d6). Il suffit de charger cette couche dans QGIS puis de sélectionner les stations pour lesquelles les mesures sont désirées. Cette sélection peut se faire à la main ou par une requête dans QGIS. Lorsqu'au moins une station est sélectionnée, ouvrez le plugin et renseignez cette couche de stations dans le champ "*Couche des stations*". Ensuite, sélectionnez le nom du champ qui contient les codes Sandre des stations dans le champ "*Champ du code des stations*". Si vous utilisez la couche initiale du Sandre, ce champ devrait se nommer
"*CdStationM*".

## La période temporelle
La période temporelle des données à récupérer doit être définie par la date de début et la date de fin. Ces dates sont à renseigner respectivement dans le cadre "*Période souhaitée*".

## Les données de physicochimie
Les données de Naïades sont discrétisées en quatre grandes classes : physicochimie, hydrobiologie, température et hydromorphologie.

Pour les deux premières classes, les données sont structurées selon la typologie du Sandre. Chaque donnée mesurée correspond à un *paramètre*, mesuré sur un *support* et sur une *fraction*.  L'Ifremer propose un [interface en ligne pour trouver les codes Sandre facilement](https://wwz.ifremer.fr/quadrige2_support/Mes-referentiels/Je-recherche-un-code-Sandre). Au moins un paramètre, ou un support ou une fraction doivent être renseignées.

### Le paramètre
Le paramètre correspond au code de l'élément mesuré (nitrate, ammonium, mercure, ...). Il est possible de renseigner le code à la main via le champ de texte libre. Si plusieurs paramètres sont souhiatés, il est nécessaire de les séparer par une virgule sans espaces. Certains paramètres fréquemment demandés sont disponibles via les boîtes à cocher du cadre *Paramètres*.

### Le support
Dans cette partie, il est nécessaire de renseigner le support sur lequel le paramètre doit être mesuré. Il s'agît le plus fréquemment de l'eau, de l'air, de sédiments, ... L'utilisateur peut, là aussi, entrer librement le code Sandre du ou des support désirés dans le champ de texte ou bien sélectionner le support via les cases à cocher. Si aucun support n'est explicitement renseigné, ils seront tous pris en compte dans la requête.

### La fraction
Le dernier cadre permet à l'utilisateut de spécifier sur quelle fraction la mesure doit avoir lieu. Par exemple, si le support désiré est l'*eau*, alors la fraction pourra être *eau brute* ou *eau filtrée*. L'utilisateur peut entrer le code Sandre de la fraction à la main ou bien cocher la fraction désirée. Si aucune fraction n'est précisée, elles seront toutes prises en compte au moment de la requête.

## Les données d'hydrobiologie
Le principe de téléchargement des données est le même que pour les données de physicochimie. La seule différence est qu'on parle de "*taxons*", "*support*" et "*indices*". Au moins un de ces trois champs doit être renseigné. Si un champ n'est pas renseigné, toutes les occurrences possibles de ce champ seront prises en compte au moment de la requête.

## Les données de température et d'hydromorphologie
Pour ces deux catégories, il n'y a pas de structuration particulière. Il suffit de cocher ce qui est désiré.

## Les résultats
Une fois les différents paramètres renseignés, il suffit de cliquer sur "*OK*" pour lancer la requête sur le serveur Naïades. Le navigateur Internet par défaut s'ouvre sur le site de Naïades. Un récapitulatif de la recherche est visible et le téléchargement du résultat est tout de suite proposé. Le fichier se nomme "*naiades_export.zip*". Les mesures se trouvent dans ce fichier compressé et plus précisément dans le fichier texte "*Analyses.csv*". D'autres fichiers apportant des précisions sur les résultats accompagnent de fichier.

L'onglet "*Lisez moi*" du module reprend ces explications.

# Limites
Il existe une limite de taille d'export de données. Si trop de stations sont sélectionnées, ou si il y a trop de données sur une trop longue période ou trop de paramètres sont demandés, la requâte n'aboutira pas. Le fichier téléchargé se nommera alors "*Documents.zip*" et ne sera pas exploitable. Il sera dans ce cas nécessaire d'effectuer sa requête en plusieurs fois.

# Crédits et contacts
Les données sont stockées sur le site de Naïades et gérées par [l'OFB](https://ofb.gouv.fr/). Veuillez vous référer aux [mentions légales de Naïades](http://naiades.eaufrance.fr/mentions-legales) pour en savoir plus.

Si vous souhaitez signaler un bug ou proposer une amélioration, n'hésitez pas à <a href="mailto:paul.passy@upmc.fr, sylvain.thery@upmc.fr">contacter l'équipe de développement.</a>