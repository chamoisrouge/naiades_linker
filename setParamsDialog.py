from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QComboBox, QDateEdit, QDialogButtonBox, QTextEdit, QCheckBox
from qgis.PyQt import uic, QtWidgets
import os

from qgis._gui import QgsMapLayerComboBox, QgsFieldComboBox

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'setParams.ui'), resource_suffix='')

class winParams(QtWidgets.QDialog, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(winParams, self).__init__(parent)
        self.iface = iface
        #  fictive objects to have the auto-completion
        self.mMapLayerComboBoxStation = QgsMapLayerComboBox()
        self.mFieldComboBoxId = QgsFieldComboBox()
        self.textEditParam = QTextEdit()
        self.checkBoxNitrates = QCheckBox()
        self.startDate = QDateEdit()
        self.endDate = QDateEdit()
        self.comboBoxQualif = QComboBox()
        self.comboBoxStatut = QComboBox()
        self.buttonBox = QDialogButtonBox()
        self.setupUi(self)

        # the title of the windows
        self.setWindowTitle("Naïades Linker")

        # init of the comboBox qualification and statut
        self.comboBoxQualif.addItems(["Indifférent","Non définissable","Correct","Incorrect","Incertaine","Non qualifié"])
        self.comboBoxStatut.addItems(["Indifférent","Donnée brute","Niveau 1","Niveau 2","Donnée interprétée"])

        # Gestion des boutons OK / annuler
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        self.buttonBox.accepted.connect(self.boutonOk)
        self.buttonBox.rejected.connect(self.boutonCancel)

        # Params to be filled
        # Layer and field
        self.mMapLayerComboBoxStation.layerChanged.connect(self.mFieldComboBoxId.setLayer)  # setLayer is a native slot function
        self.mMapLayerComboBoxStation.layerChanged.connect(self.validValues)
        self.mFieldComboBoxId.fieldChanged.connect(self.validValues)

        # dates
        self.endDate.dateTimeChanged.connect(self.validValues)
        self.startDate.dateTimeChanged.connect(self.validValues)

        # some checks...
        self.mMapLayerComboBoxStation.currentLayer()
        self.checkOut = False # pour signaler si on continue

    def validValues(self):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        if self.mFieldComboBoxId.currentIndex() == -1:
            return False

        if self.endDate.date().toPyDate() < self.startDate.date().toPyDate():
            return False

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)


    def boutonCancel(self):
        """
        On ferme le dialog sans rien faire
        :return:
        """
        self.checkOut = False
        self.close()

    def boutonOk(self):
        """
        We close the dialog and we launch the process
        :return:
        """
        self.checkOut = True
        self.close()
