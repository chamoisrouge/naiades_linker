<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="fr_FR">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../setParams.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="129"/>
        <source>Couche des stations</source>
        <translation type="unfinished">Layer of stations</translation>
    </message>
    <message>
        <location filename="../setParams.py" line="60"/>
        <source>Champ avec ID</source>
        <translation type="unfinished">ID field</translation>
    </message>
    <message>
        <location filename="../setParams.py" line="61"/>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.py" line="62"/>
        <source>End date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.py" line="63"/>
        <source>Thématique</source>
        <translation type="unfinished">Thematic</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="59"/>
        <source>Période souhaitée</source>
        <translation type="unfinished">Studied period</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="80"/>
        <source>Date de début</source>
        <translation type="unfinished">Start date</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="87"/>
        <source>Date de fin</source>
        <translation type="unfinished">End date</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="112"/>
        <source>Couche et code des stations</source>
        <translation type="unfinished">Layer and field of stations codes</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="119"/>
        <source>Champ du code des staions</source>
        <translation type="unfinished">Field of stations codes</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="146"/>
        <source>Physicochimie</source>
        <translation type="unfinished">Physico chemistry</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="164"/>
        <source>Paramètres</source>
        <translation type="unfinished">Params</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="184"/>
        <source>Paramètre(s) Sandre</source>
        <translation type="unfinished">Sandre params</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="195"/>
        <source>Nitrates (1340)</source>
        <translation type="unfinished">Nitrates (1340)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="205"/>
        <source>P total (1350)</source>
        <translation type="unfinished">Total P (1350)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="212"/>
        <source>MES (1305)</source>
        <translation type="unfinished">SPM (1305)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="219"/>
        <source>Aluminium (1370)</source>
        <translation type="unfinished">Aluminium (1370)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="226"/>
        <source>Nitrites (1339)</source>
        <translation type="unfinished">Nitrites (1339)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="233"/>
        <source>Silice (1348)</source>
        <translation type="unfinished">Silica (1348)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="240"/>
        <source>Fer (1393)</source>
        <translation type="unfinished">Iron (1393)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="247"/>
        <source>Magnésium (1372)</source>
        <translation type="unfinished">Magnesium (1372)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="254"/>
        <source>Ammonium (1335)</source>
        <translation type="unfinished">Ammonium (1335)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="261"/>
        <source>Oxygène dissous (1311)</source>
        <translation type="unfinished">Dissolved oxygen (1311)</translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="268"/>
        <source>Plomb (1382)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="275"/>
        <source>Cadmium (1398)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="282"/>
        <source>Ortho-phosphates (1433)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="289"/>
        <source>C organique (1841)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="296"/>
        <source>Mercure (1387)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="303"/>
        <source>HAP (62)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="522"/>
        <source>Supports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="534"/>
        <source>Support(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="353"/>
        <source>Sol (25)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="360"/>
        <source>Sédiments (6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="367"/>
        <source>Air (2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="374"/>
        <source>Eau (3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="398"/>
        <source>Fractions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="413"/>
        <source>Fraction(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="424"/>
        <source>Eau brute (23)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="431"/>
        <source>Sédiments bruts (31)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="438"/>
        <source>Air brut (19)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="445"/>
        <source>MES brutes (41)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="452"/>
        <source>Testicules de pinnipède (212)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="465"/>
        <source>Hydrobiologie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="483"/>
        <source>Taxons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="495"/>
        <source>Taxon(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="548"/>
        <source>Diatomées benthiques  (10)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="558"/>
        <source>Macrophytes (27)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="565"/>
        <source>Phytoplancton (11)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="572"/>
        <source>Macroinvertébrés aquatiques (13)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="579"/>
        <source>Poissons (4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="603"/>
        <source>Indices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="615"/>
        <source>Indice(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="629"/>
        <source>Ind. Poissons Rivières (7036)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="636"/>
        <source>NER (7744)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="643"/>
        <source>DIO (7746)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="650"/>
        <source>DTI (7787)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="657"/>
        <source>NEL (7743)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="664"/>
        <source>DII (7745)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="671"/>
        <source>DIT (7786)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="678"/>
        <source>NTE (7644)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="718"/>
        <source>Température</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="728"/>
        <source>Température (1301)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="774"/>
        <source>Hydromorphologie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="786"/>
        <source>Lisez moi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="804"/>
        <source>Il est nécessaire de charger dans QGIS la couche des stations de mesures de qualité, librement téléchargeable sur le site du Sandre en cliquant sur le lien suivant :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="823"/>
        <source>Ce plugin permet de télécharger facilement les données relatives à la qualité des cours d’eau français en interrogeant la base Naïades (Agence Française pour la Biodiversité, EauFrance, Sandre). Il s’agît simplement d’un interface construisant un URL intelligible par</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="846"/>
        <source>Utilisation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="869"/>
        <source>But</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="891"/>
        <source>L’utilisateur doit ensuite sélectionner les stations d’intérêt grâce à l’outil de sélection de QGIS ou par une requête SQL.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="916"/>
        <source>Une fois cette sélection effectuée et le plugin lancé, il est tout d’abord nécessaire d’initialiser la couche des stations de mesures de qualité et le champ contenant le code des stations. Dans la couche d’origine, ce champ se nomme « CdStationM ». La période à télécharger doit ensuite être renseignée à l’aide de dates de début et de fin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="941"/>
        <source>Une fois cette sélection effectuée, il est nécessaire de renseigner les variables à télécharger. Les quatre grands groupe de variables correspondent aux quatre onglets : physicochimie, hydrobiologie, température et hydromorphologie.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="966"/>
        <source>Pour les deux premiers groupes, il est possible soit de renseigner les paramètres, les supports et les fractions directement à partir de leur code Sandre dans les zones de texte dédiées. Si plusieurs codes sont entrés dans une même case, ils doivent être séparés par des virgules sans espace. Les codes les plus utilisés sont proposés directement sous forme de cases à cocher. Lorsqu’une ou plusieurs cases sont cochées il n’est pas nécessaire de renseigner de codes dans la zone de texte.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="991"/>
        <source>Il est possible de combiner un paramètre avec un support et une fraction. Il est également possible de ne renseigner qu’un seul champ sur les trois.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="1016"/>
        <source>Lorsque tous les renseignements sont renseignés et que l’utilisateur clique sur OK, le navigateur par défaut ouvre automatiquement l’URL de l’API Naïades et lance le téléchargement des données demandées au format csv dans un fichier compressé.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="1035"/>
        <source>&lt;a href=&quot;http://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/71767e88-a021-4e88-8787-5feed04958d6&quot; style=&quot;color:#7BA11A;text-decoration:none;&quot;&gt;Stations de mesure de la qualité des eaux superficielles continentales&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setParams.ui" line="1051"/>
        <source>&lt;a href=&quot;http://naiades.eaufrance.fr/gestion-url&quot; style=&quot;color:#7BA11A;text-decoration:none;&quot;&gt;l&apos;API Naïades.&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NaidesLinker</name>
    <message>
        <location filename="../naiades_linker.py" line="184"/>
        <source>&amp;Naiades Downloader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../naiades_linker.py" line="170"/>
        <source>NaÃ¯ades Downloader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../naiades_linker.py" line="220"/>
        <source>%s invalid layer </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../naiades_linker.py" line="226"/>
        <source>pyNutsGUI Plugin error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../naiades_linker.py" line="221"/>
        <source>%s: invalid layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../naiades_linker.py" line="226"/>
        <source>%s layer not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
