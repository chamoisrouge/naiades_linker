#!/usr/bin/env bash
# -*- mode:shell-script; coding:utf-8; -*-
# ---------------------------------------------
# Creates a symlink to this directory in QGIS3 python plugins default profile directory

ln -s $(pwd) /home/$USER/.local/share/QGIS/QGIS3/profiles/default/python/plugins/naiades_linker

