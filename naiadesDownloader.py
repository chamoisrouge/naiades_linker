from qgis._core import QgsVectorLayer

from .setParamsDialog import winParams
from PyQt5.QtWidgets import QComboBox, QDateEdit, QDialogButtonBox
from qgis._gui import QgsMapLayerComboBox, QgsFieldComboBox
import webbrowser

def go(self):
    """
    In this function we launch the dialog through 'winParams' and we grab back the params
    :param self:
    :return:
    """
    dlg = winParams(self.iface)
    dlg.exec_()

    # if dlg.accepted():
    #     print("ok")
    if not dlg.checkOut:
        print("Annuler")
        return False

    ### Unvariant params ###
    #  type of results
    viz = 'resultats'
    # a list to store the thematics
    lstThematic = []


    ################################## Layer and field ##################################
    # we grab the layer which is the one selected in the dlg.mMapLayerComboBoxStation
    layer = dlg.mMapLayerComboBoxStation.currentLayer()
    # we grab the field
    field = dlg.mFieldComboBoxId.currentField()

    ################################## Dates ##################################
    # we grab the dates and we store them in PyDate format
    startDate = dlg.startDate.date().toPyDate()
    endDate = dlg.endDate.date().toPyDate()
    # we transform the dates into strings like this 'dd-mm-yyyy'
    startDate = transformDate(startDate)
    endDate = transformDate(endDate)

    ################################## Physicochimie - Params ##################################
    # we grab the params
    # if the user filled the list
    param = dlg.textEditParam.toPlainText()
    # if the user checked some boxes
    lstParam = []
    if dlg.checkBoxNitrates.isChecked():
        lstParam.append('1340')
    if dlg.checkBoxNitrites.isChecked():
        lstParam.append('1339')
    if dlg.checkBoxAmmonium.isChecked():
        lstParam.append('1335')
    if dlg.checkBoxPhosphate.isChecked():
        lstParam.append('1433')
    if dlg.checkBoxPtot.isChecked():
        lstParam.append('1350')
    if dlg.checkBoxOxygene.isChecked():
        lstParam.append('1311')
    if dlg.checkBoxSilice.isChecked():
        lstParam.append('1348')
    if dlg.checkBoxCorg.isChecked():
        lstParam.append('1841')
    if dlg.checkBoxMES.isChecked():
        lstParam.append('1305')
    if dlg.checkBoxFer.isChecked():
        lstParam.append('1393')
    if dlg.checkBoxPlomb.isChecked():
        lstParam.append('1382')
    if dlg.checkBoxMercure.isChecked():
        lstParam.append('1387')
    if dlg.checkBoxAlu.isChecked():
        lstParam.append('1370')
    if dlg.checkBoxMagnesium.isChecked():
        lstParam.append('1372')
    if dlg.checkBoxHAP.isChecked():
        lstParam.append('62')
    if dlg.checkBoxCadmium.isChecked():
        lstParam.append('1398')
    # if the params were entered through the checkboxes we build the 'param' variable
    if len(lstParam) > 0:
        param = ','.join(lstParam)

    ################################## Physicochimie - Support ##################################
    # we grab the support
    # in the case the user entered free text
    support = dlg.textEditSupport.toPlainText()
    # in the case the user checked the checkboxes
    lstSupport = []
    if dlg.checkBoxSupEau.isChecked():
        lstSupport.append('3')
    if dlg.checkBoxSupAir.isChecked():
        lstSupport.append('2')
    if dlg.checkBoxSupSediments.isChecked():
        lstSupport.append('6')
    if dlg.checkBoxSupSol.isChecked():
        lstSupport.append('25')
    # if the supports were entered through the checkboxes we build the 'support' variable
    if len(lstSupport) > 0:
        support = ','.join(lstSupport)

    ################################## Physicochimie - Fraction ##################################
    # we grab the fraction
    # in the case the user entered free text
    fraction = dlg.textEditFraction.toPlainText()
    # in the case the user checked the checkboxes
    lstFraction = []
    if dlg.checkBoxFracEau.isChecked():
        lstFraction.append('23')
    if dlg.checkBoxFracAir.isChecked():
        lstFraction.append('19')
    if dlg.checkBoxFracSediments.isChecked():
        lstFraction.append('31')
    if dlg.checkBoxFracMES.isChecked():
        lstFraction.append('41')
    if dlg.checkBoxFracTesti.isChecked():
        lstFraction.append('212')
    # if the fractions were entered through the checkboxes we build the 'fraction' variable
    if len(lstFraction) > 0:
        fraction = ','.join(lstFraction)

    # if 'param', 'support' or 'fraction' are not empty, the thematic is set to physicochimie'
    if param != '' or support != '' or fraction != '':
        lstThematic.append('physicochimie')


    ################################## Temperature ##################################
    if dlg.checkBoxTemperature.isChecked():
        lstThematic.append('temperature')


    ################################## Temperature ##################################
    if dlg.checkBoxHydromorpho.isChecked():
        lstThematic.append('hydromorphologie')


    ################################## Hydrobiologie - Taxon ##################################
    # we grab the taxons
    # if the user filled the taxons list
    taxon = dlg.textEditTaxon.toPlainText()

    ################################## Hydrobiologie - Support ##################################
    # we grab the support of hydrobiologie
    # in the case the user entered free text
    supportBio = dlg.textEditSupportBio.toPlainText()
    # in the case the user checked the checkboxes
    lstSupportBio = []
    if dlg.checkBoxDiaBenth.isChecked():
        lstSupportBio.append('10')
    if dlg.checkBoxPhyto.isChecked():
        lstSupportBio.append('11')
    if dlg.checkBoxMacrophyte.isChecked():
        lstSupportBio.append('27')
    if dlg.checkBoxMacroInv.isChecked():
        lstSupportBio.append('13')
    if dlg.checkBoxPoissons.isChecked():
        lstSupportBio.append('4')
    # if the supports were entered through the checkboxes we build the 'supportBio' variable
    if len(lstSupportBio) > 0:
        supportBio = ','.join(lstSupportBio)

    ################################## Hydrobiologie - Indices ##################################
    # we grab the indices
    # in the case the user entered free text
    indice = dlg.textEditSupportIndice.toPlainText()
    # in the case the user checked the checkboxes
    lstIndice = []
    if dlg.checkBoxIPR.isChecked():
        lstIndice.append('7036')
    if dlg.checkBoxNEL.isChecked():
        lstIndice.append('7743')
    if dlg.checkBoxNER.isChecked():
        lstIndice.append('7744')
    if dlg.checkBoxDII.isChecked():
        lstIndice.append('7745')
    if dlg.checkBoxDIO.isChecked():
        lstIndice.append('7746')
    if dlg.checkBoxDIT.isChecked():
        lstIndice.append('7786')
    if dlg.checkBoxDTI.isChecked():
        lstIndice.append('7787')
    if dlg.checkBoxNTE.isChecked():
        lstIndice.append('7644')
    # if the indices were entered through the checkboxes we build the 'indices' variable
    if len(lstIndice) > 0:
        indice = ','.join(lstIndice)

    # if taxon or support bio or indices fields are not empty, we set the thematic to 'hydrobiologie'
    if taxon != '' or supportBio != '' or indice != '':
        lstThematic.append('hydrobiologie')


    ################################## Qualification ##################################
    # we grab the qualification from the comboBox
    qualification = dlg.comboBoxQualif.currentText()
    if qualification == 'Indifférent':
        qualification = ''
    elif qualification == 'Non définissable':
        qualification = '0'
    elif qualification == 'Correct':
        qualification = '1'
    elif qualification == 'Incorrect':
        qualification = '2'
    elif qualification == 'Incertaine':
        qualification = '3'
    elif qualification == 'Non qualifié':
        qualification = '4'

    ################################## Statut ##################################
    # we grab the statut from the comboBox
    statut = dlg.comboBoxStatut.currentText()
    if statut == 'Indifférent':
        statut = ''
    elif statut == 'Donnée brute':
        statut = '1'
    elif statut == 'Niveau 1':
        statut = '2'
    elif statut == 'Niveau 2':
        statut = '3'
    elif statut == 'Donnée interprétée':
        statut = '4'


    ################################## Building of the URL ##################################
    #  we build the Naiades URL according to the params
    # we browse the thematics (several thematics may have been set)
    for thematic in lstThematic:
        if thematic == 'physicochimie':
            naiadesUrl = buildUrl(layer, field, startDate, endDate, thematic, viz, qualification, statut, param, fraction, support, qualification, statut)
            print(naiadesUrl)
        if thematic == 'temperature' or thematic == 'hydromorphologie':
            naiadesUrl = buildUrl(layer, field, startDate, endDate, thematic, viz, qualification, statut)
            print(naiadesUrl)
        if thematic == 'hydrobiologie':
            naiadesUrl = buildUrl(layer, field, startDate, endDate, thematic, viz, qualification, statut, taxon, supportBio, indice)
            print(naiadesUrl)

def transformDate(userDate):
    """
    A function to transfrom the dates in string in the correct format
    :param self: a PyDate
    :return: a string of the date in the correct format 'dd-mm-yyyy'
    """
    # we build the string
    if userDate.day < 10:
        dd = '0' + str(userDate.day)
    else:
        dd = str(userDate.day)
    if userDate.month < 10:
        mm = '0' + str(userDate.month)
    else:
        mm = str(userDate.month)
    yyyy = str(userDate.year)

    dateOk = dd + '-' + mm + '-' + yyyy
    return dateOk


def buildUrl(layer, field, startDate, endDate, thematic, viz, qualification, statut, param = '', fraction = '', support = '', taxon = '', supportBio = '', indice = ''):
    """
    A function to build the Naiades URL which allows the user to download the data he wants in csv format
    :param self: layer, field, startDate, endDate, thematic
    :return: an URL in string
    """
    # a list to store the id of selected stations
    lstStat = []
    for s in layer.getSelectedFeatures():
        lstStat.append(s[field])

    # we build the components of the URL
    # we build the URL part corresponding to the stations
    partStation = ','.join(lstStat)

    # the date
    if startDate != '':
        period = 'debut=' + startDate + '&fin=' + endDate
    else:
        period = ''

    # the params
    if param != '':
        param = '&parametres=' + param
    else:
        param = param

    # the support
    if support != '':
        support = '&supports=' + support
    else:
        support = support

    # the fraction
    if fraction != '':
        fraction = '&fractions=' + fraction
    else:
        fraction = fraction

    # the qualification
    if qualification != '':
        qualification = '&qualifications=' + qualification
    else:
        qualification = qualification

    # the statut
    if statut != '':
        statut = '&statuts=' + statut
    else:
        statut = statut

    # the taxon
    if taxon != '':
        taxon = '&taxons=' + taxon
    else:
        taxon = taxon

    # the support bio
    if supportBio != '':
        supportBio = '&supports=' + supportBio
    else:
        supportBio = supportBio

    # the indice
    if indice != '':
        indice = '&indices=' + indice
    else:
        indice = indice


    #  we build the final URL
    urlStation = 'http://naiades.eaufrance.fr/acces-donnees#/' + thematic + '/' + viz + '/exportCsv?' + period + '&stations=' + partStation + \
                 param + support + fraction + taxon + supportBio + indice +\
                 statut + qualification
    print(urlStation)
    # we open the URL
    webbrowser.open(urlStation)

    return urlStation